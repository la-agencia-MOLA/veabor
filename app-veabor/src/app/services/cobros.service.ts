import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { CobroModel } from '../models/cobros.model';
import { GlobalService, Varglobal } from './global.service';
//import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 't0k3n001'
  })
};


@Injectable()
export class CobrosService {

  //variable global
  varglobal:Varglobal[] = [];

  //url de la base de datos donde se guarda el cobro

  constructor( private http:HttpClient, private _varglobal:GlobalService ) { }


  crearCobro( forma:CobroModel ){
    this.varglobal = this._varglobal.getGlobales();

    //console.log(this.url);
    return this.http.post(`${ this.varglobal[0].url }/cobros/guardar`, forma, httpOptions);
    //return this.http.post(this.url, forma);


  }

}
