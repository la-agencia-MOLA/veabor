import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {
  private varglobal:Varglobal[] = [
    {
      id: 0,
      name: 'url API rest splitflare',
      url: 'http://192.168.0.9:8000'
    }
  ];


  constructor() { }

  getGlobales():Varglobal[]{
    return this.varglobal;
  }
}

export interface Varglobal{
  id: number;
  name: string;
  url: string;
};
