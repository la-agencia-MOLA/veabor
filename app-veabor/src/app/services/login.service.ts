import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from '../models/usuario.model';
//import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 't0k3n001'
  })
};


@Injectable()
export class LoginService {



  //url de la base de datos donde se guarda el cobro
  private url = 'http://192.168.0.15:8000/usuarios/login';

  constructor( private http:HttpClient ) { }

  iniciaLogin( loginData:UsuarioModel ){
    //console.log(this.url);
    return this.http.post(this.url, loginData, httpOptions);
    //return this.http.post(this.url, forma);


  }

}
