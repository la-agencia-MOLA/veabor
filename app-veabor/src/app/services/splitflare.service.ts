import { Injectable } from '@angular/core';

@Injectable()
export class SplitflareService {
  private splitflares:Splitflare[] = [
    {
      id: 4,
      user: 'Cristian Vanegas',
      date:'01-08-2019',
      status: 'pagado',
      title: 'Compra Guaro',
      travel: 'Viaje Mola',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 150000,
      statusSplitflare: 'cobro',
      coin: 'COP'
    },
    {
      id: 2500,
      user: 'Diana Espitia',
      date:'05-08-2019',
      status: '',
      title: 'Almuerzo',
      travel: 'Viaje Mola',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 8000,
      statusSplitflare: 'pago',
      coin: 'COP'
    },
    {
      id: 58,
      user: 'Harold Cardona',
      date:'15-07-2019',
      status: 'borrador',
      title: 'Recuerdos',
      travel: 'Viaje Mola',
      description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      price: 50000,
      statusSplitflare: 'historial',
      coin: 'COP'
    }
  ];
  constructor() {
    //console.log('servicio listo para usarse');
  }

  getCobros():Splitflare[]{
    return this.splitflares;
  }
  //obtiene no un ID sino la posición del arreglo del objeto
  getCobro( idx: number ){
    let res = this.splitflares.find(res => res.id == idx);
    return res;
  }
}

export interface Splitflare{
  id: number;
  user: string;
  date: string;
  status: string;
  title: string;
  travel: string;
  description: string;
  price: number;
  statusSplitflare: string;
  coin: string;
};
