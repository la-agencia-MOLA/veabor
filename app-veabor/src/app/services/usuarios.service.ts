import { Injectable } from '@angular/core';

@Injectable()
export class UsuariosService {

  private usuarios:Usuario[] = [
    {
      nombre: 'Felipe',
      apellido: 'Cancino',
      imagen: 'assets/images/foto-felipe.jpeg',
      admin: true
    },{
      nombre: 'Cristian',
      apellido: 'Vanegas',
      imagen: 'assets/images/foto-cristian.jpeg',
      admin: false
    },{
      nombre: 'Harold',
      apellido: 'Cardona',
      imagen: 'assets/images/foto-harold.png',
      admin: false
    },{
      nombre: 'Diana',
      apellido: 'Espitia',
      imagen: 'assets/images/foto-diana.jpg',
      admin: false
    }
  ];

  constructor() {
    //console.log('servicio listo para usarse');
  }

  getUsuarios():Usuario[]{
    return this.usuarios;
  }

}

export interface Usuario{
  nombre: string;
  apellido: string;
  imagen: string;
  admin: boolean;
};
