import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//rutas
import { APP_ROUTING } from './app.routes';

//servicio
import { UsuariosService } from './services/usuarios.service';
import { SplitflareService } from './services/splitflare.service';
import { CobrosService } from './services/cobros.service';
import { LoginService } from './services/login.service';
import { GlobalService } from './services/global.service';

//componentes
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SplitflareComponent } from './components/splitflare/splitflare.component';
import { CobrosComponent } from './components/splitflare/cobros/cobros.component';
import { NuevoCobroComponent } from './components/splitflare/cobros/nuevo-cobro/nuevo-cobro.component';
import { PagosComponent } from './components/splitflare/pagos/pagos.component';
import { HistorialComponent } from './components/splitflare/historial/historial.component';
import { DetalleCobroComponent } from './components/splitflare/cobros/detalle-cobro/detalle-cobro.component';
import { DetallePagoComponent } from './components/splitflare/pagos/detalle-pago/detalle-pago.component';
import { DetalleHistorialComponent } from './components/splitflare/historial/detalle-historial/detalle-historial.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    SplitflareComponent,
    CobrosComponent,
    NuevoCobroComponent,
    PagosComponent,
    HistorialComponent,
    DetalleCobroComponent,
    DetallePagoComponent,
    DetalleHistorialComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [
    UsuariosService,
    SplitflareService,
    CobrosService,
    LoginService,
    GlobalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
