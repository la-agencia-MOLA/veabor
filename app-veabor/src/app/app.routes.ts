import { RouterModule, Routes } from '@angular/router';
import { SplitflareComponent } from './components/splitflare/splitflare.component';
import { CobrosComponent } from './components/splitflare/cobros/cobros.component';
import { NuevoCobroComponent } from './components/splitflare/cobros/nuevo-cobro/nuevo-cobro.component';
import { DetalleCobroComponent } from './components/splitflare/cobros/detalle-cobro/detalle-cobro.component';
import { PagosComponent } from './components/splitflare/pagos/pagos.component';
import { DetallePagoComponent } from './components/splitflare/pagos/detalle-pago/detalle-pago.component';
import { HistorialComponent } from './components/splitflare/historial/historial.component';
import { DetalleHistorialComponent } from './components/splitflare/historial/detalle-historial/detalle-historial.component';
import { LoginComponent } from './components/login/login.component';

const APP_ROUTES: Routes = [
  { path: 'splitflare', component: SplitflareComponent },
  { path: 'splitflare/cobros', component: CobrosComponent },
  { path: 'splitflare/cobros/detalle-cobro/:id', component: DetalleCobroComponent },
  { path: 'splitflare/cobros/nuevo-cobro', component: NuevoCobroComponent },
  { path: 'splitflare/pagos', component: PagosComponent },
  { path: 'splitflare/pagos/detalle-pago/:id', component: DetallePagoComponent },
  { path: 'splitflare/historial', component: HistorialComponent },
  { path: 'splitflare/historial/detalle-historial/:id', component: DetalleHistorialComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'splitflare' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

//constante routing con HASH ( # ) se usa por si la url llevará parámetros
//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash:true });
