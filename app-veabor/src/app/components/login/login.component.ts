import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from '../../models/usuario.model';
import { HttpHeaders } from '@angular/common/http';
import { LoginService } from '../../services/login.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;
  loginUser: UsuarioModel = new UsuarioModel();

  constructor( private loginService:LoginService ) { }

  ngOnInit() {

    this.usuario = new UsuarioModel();
    this.usuario.usuario = 'Felipe';

  }

  login( loginData:NgForm ) {
    if( loginData.invalid ){
      console.log('formulario no válido');
      return;
    }

    //console.log(this.loginUser);
    this.loginService.iniciaLogin( this.loginUser )
      .subscribe( resp => {
        console.log('respuesta login Angular', resp);
      });
  }

}
