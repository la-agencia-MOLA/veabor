import { Component, OnInit } from '@angular/core';
// import { UsuariosService, Usuario } from '../../../services/usuarios.service';
// import { SplitflareService, Splitflare } from '../../../services/splitflare.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.scss']
})
export class HistorialComponent implements OnInit {

  datos:any[] = [];

  constructor( private http:HttpClient ) {
    this.http.get('http://192.168.0.15:8000/cobros/')
      .subscribe( (data:any) => {
        this.datos = data;
      })
  }

  ngOnInit() {

  }

}
