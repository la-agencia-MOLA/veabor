import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuariosService, Usuario } from '../../../../services/usuarios.service';
import { SplitflareService, Splitflare } from '../../../../services/splitflare.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-detalle-historial',
  templateUrl: './detalle-historial.component.html',
  styleUrls: ['./detalle-historial.component.scss']
})
export class DetalleHistorialComponent implements OnInit {
  //declaracion de variables de los servicios
  datos: any[] = [];
  cobro:any = {};

  usuarios:Usuario[] = [];

  splitflares:Splitflare[] = [];

  constructor( private activatedRoute:ActivatedRoute, private _usuariosService:UsuariosService, private _splitflareService:SplitflareService, private http:HttpClient ) {
    this.activatedRoute.params.subscribe( params => {
      this.http.get('http://192.168.0.15:8000/cobros/' + params['id'] )
        .subscribe( (data: any) => {
          this.datos = data;
          //console.log(data);
          //console.log(data[3]['id']);
        })
    })
  }

  ngOnInit() {
    //servicio usuarios
    this.usuarios = this._usuariosService.getUsuarios();
    //servicio cobros
    this.splitflares = this._splitflareService.getCobros();
    //console.log(this.splitflares);
  }

}
