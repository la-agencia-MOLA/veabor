import { Component, OnInit } from '@angular/core';
// import { UsuariosService, Usuario } from '../../../services/usuarios.service';
// import { SplitflareService, Splitflare } from '../../../services/splitflare.service';
import { HttpClient } from '@angular/common/http';
import { GlobalService, Varglobal } from '../../../services/global.service';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.html',
  styleUrls: ['./pagos.component.scss']
})
export class PagosComponent implements OnInit {

  //declaracion de variables de los servicios
  datos: any[] = [];
  varglobal:Varglobal[] = [];

  constructor( private http:HttpClient, private _varglobal:GlobalService ) {
    this.varglobal = this._varglobal.getGlobales();
    //this.http.get('http://192.168.0.15:8000/cobros/')
    this.http.get(`${ this.varglobal[0].url }/cobros/`)
      .subscribe( (data:any) => {
          this.datos = data;
      })
  }

  ngOnInit() {


  }

}
