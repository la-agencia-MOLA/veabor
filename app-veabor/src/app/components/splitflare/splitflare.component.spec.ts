import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplitflareComponent } from './splitflare.component';

describe('SplitflareComponent', () => {
  let component: SplitflareComponent;
  let fixture: ComponentFixture<SplitflareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplitflareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplitflareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
