import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsuariosService, Usuario } from '../../../../services/usuarios.service';
import { SplitflareService, Splitflare } from '../../../../services/splitflare.service';
import { HttpClient } from '@angular/common/http';
import { GlobalService, Varglobal } from '../../../../services/global.service';

@Component({
  selector: 'app-detalle-cobro',
  templateUrl: './detalle-cobro.component.html',
  styleUrls: ['./detalle-cobro.component.scss']
})
export class DetalleCobroComponent implements OnInit {

  //declaracion de variables de los servicios
  varglobal:Varglobal[] = [];
  datos: any[] = [];
  cobro:any = {};

  usuarios:Usuario[] = [];

  splitflares:Splitflare[] = [];

  constructor( private activatedRoute:ActivatedRoute, private _usuariosService:UsuariosService, private _splitflareService:SplitflareService, private http:HttpClient, private _varglobal:GlobalService ) {
    this.activatedRoute.params.subscribe( params => {
        //this.cobro = this._splitflareService.getCobro( params['id'] );//me trae la data del id del cobro unicamente
        //console.log(params['id']);
        this.varglobal = this._varglobal.getGlobales();
        //this.http.get('http://192.168.0.15:8000/cobros/' + params['id'] )
        this.http.get(`${ this.varglobal[0].url }/cobros/${ params['id'] }`)
          .subscribe( (data: any) => {
            this.datos = data;
            //console.log(data);
            //console.log(data[3]['id']);
          })
    })

  }

  ngOnInit() {
    //servicio usuarios
    this.usuarios = this._usuariosService.getUsuarios();
    //servicio cobros
    this.splitflares = this._splitflareService.getCobros();
    //console.log(this.splitflares);
  }

}
