import { Component, OnInit } from '@angular/core';
// import { UsuariosService, Usuario } from '../../../services/usuarios.service';
// import { SplitflareService, Splitflare } from '../../../services/splitflare.service';
import { HttpClient } from '@angular/common/http';
import { GlobalService, Varglobal } from '../../../services/global.service';

@Component({
  selector: 'app-cobros',
  templateUrl: './cobros.component.html',
  styleUrls: ['./cobros.component.scss']
})
/* -->>> clase comentada, ya que acá carga el servicio cobros con el json local
export class CobrosComponent implements OnInit {

  //declaracion de variables de los servicios
  usuarios:Usuario[] = [];

  splitflares:Splitflare[] = [];

  constructor( private _usuariosService:UsuariosService, private _splitflareService:SplitflareService ) { }

  ngOnInit() {
    //servicio usuarios
    this.usuarios = this._usuariosService.getUsuarios();
    //servicio cobros
    this.splitflares = this._splitflareService.getCobros();
    //console.log(this.splitflares);
  }


}
*/

export class CobrosComponent implements OnInit {

  varglobal:Varglobal[] = [];

  datos: any[] = [];

  constructor( private http: HttpClient, private _varglobal:GlobalService ) {

    this.varglobal = this._varglobal.getGlobales();

    //this.http.get('http://192.168.0.15:8000/cobros/')
    this.http.get(`${ this.varglobal[0].url }/cobros/`)
      .subscribe( (data: any) => {
        this.datos = data;
        //console.log(data);
      })

  }

  ngOnInit() {

  }


}
