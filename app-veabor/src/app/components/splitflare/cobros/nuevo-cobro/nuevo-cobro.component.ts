import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { GlobalService, Varglobal } from '../../../../services/global.service';
import { CobrosService } from '../../../../services/cobros.service';
import { CobroModel } from '../../../../models/cobros.model';

@Component({
  selector: 'app-nuevo-cobro',
  templateUrl: './nuevo-cobro.component.html',
  styleUrls: ['./nuevo-cobro.component.scss']
})
export class NuevoCobroComponent implements OnInit {

  //data por defecto
  usuario:Object = {
    cobro: 'Proyecto',
    valor: 250000,
    moneda: 'cop',
    descripcion: 'Lorem ipsum prueba dolor',
    viaje: 'viaje planeado 3'
  }

  cobro: CobroModel = new CobroModel();




  constructor( private cobrosService:CobrosService, private _varglobal:GlobalService ) { }

  ngOnInit() {
  }

  guardaCobro( forma:NgForm ){

    if( forma.invalid ){
      console.log('formulario no válido');
      return;
    }
    //console.log(this.cobro);
    this.cobrosService.crearCobro( this.cobro )
      .subscribe( resp => {
        console.log('respuesta Angular', resp);
      });

  }

}
